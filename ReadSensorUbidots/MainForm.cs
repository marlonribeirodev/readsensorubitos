﻿using System;
using System.IO.Ports;
using System.Linq;
using System.Windows.Forms;
using Ubidots;

namespace ReadSensorUbidots
{
    public partial class MainForm : Form
    {
        #region Keys

        private readonly string apiKey = "BBFF-23996aba1dc46b9abb5800795ec97dbbf5a";

        private readonly string variableTemperatureName = "Temperatura";

        private readonly string variableSpeedName = "Velocidade";

        private readonly string dataSourceName = "Temperatura";

        #endregion

        #region Properties

        public ApiClient Api { get; set; }

        public DataSource DataSource { get; set; }

        public Variable VariableTemperature { get; set; }

        public Variable VariableSpeed { get; set; }

        #endregion

        #region Initializing

        /// <summary>
        /// Constructor.
        /// </summary>
        public MainForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Initialize component with serial port communication.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainFormLoad(object sender, EventArgs e)
        {
            Log("Initializing...");

            serialPort.DataReceived -= SerialPortDataReceived;
            serialPort.DataReceived += SerialPortDataReceived;
            FormClosing -= OnClosing;
            FormClosing += OnClosing;

            baudRateComboBox.DataSource = new string[] { "110", "300", "600", "1200", "2400", "4800", "9600", "14400",
                                                         "19200", "28800", "38400", "56000", "57600", "115200", "128000",
                                                         "153600", "230400", "256000", "460800", "921600" };
            baudRateComboBox.SelectedIndex = 6;

            var comPort = GetComPorts();
            var ubidots = false;

            if (comPort)
            {
                ubidots = SetupUbidots();
            }

            if (!comPort || !ubidots)
            {
                connectButton.Enabled = false;
                return;
            }

            Log("Successfully initialized.");
        }

        #endregion

        #region Events and Methods

    
        /// <summary>
        /// Get all available COM ports.
        /// </summary>
        /// <returns></returns>
        private bool GetComPorts()
        {
            bool result = false;

            // Gets all available COM ports
            string[] ports = SerialPort.GetPortNames();
            int portCount = ports.Count();

            if (portCount > 0)
            {
                portComboBox.DataSource = ports;
                Log(portCount == 1 ? "Successfully found 1 COM port." : string.Format("Successfully found {0} COM ports.", portCount));
                result = true;
            }
            else
            {
                Log("No COM ports are available.");
                Log("Terminating initialize.");
            }

            return result;
        }

        /// <summary>
        /// Setup Ubidots keys.
        /// </summary>
        /// <returns></returns>
        private bool SetupUbidots()
        {
            bool result = false;

            try
            {
                Log("Configuring Ubidots...");

                Api = new ApiClient(apiKey);
                var dataSources = Api.GetDataSources();

                Log(string.Format("Searching for data source '{0}'...", dataSourceName));
                var dataSource = dataSources.FirstOrDefault(d => d.GetName() == dataSourceName);

                if (dataSource == null)
                {
                    Log(string.Format("Did not find data source '{0}'. Creating one...", dataSourceName));
                    DataSource = Api.CreateDataSource(dataSourceName);
                }
                else
                {
                    DataSource = dataSource;
                }

                if (DataSource == null)
                    throw new Exception(string.Format("Could not get the '{0}' data source.", dataSourceName));

                Log(string.Format("Successfully aquired '{0}' data source.", dataSourceName));

                Log(string.Format("Searching for variable '{0}'...", variableTemperatureName));

                SetupVariables();

                result = true;
            }
            catch (Exception ex)
            {
                Log(string.Format("UbidotsConfig - ERROR: {0}", ex));
            }

            return result;
        }

        /// <summary>
        /// Setup Ubidots variables.
        /// </summary>
        private void SetupVariables()
        {
            var variableTemperature = DataSource.GetVariables().FirstOrDefault(v => v.GetName() == variableTemperatureName);
            if (variableTemperature == null)
            {
                Log(string.Format("Did not find variable '{0}'. Creating one...", variableTemperatureName));
                VariableTemperature = DataSource.CreateVariable(variableTemperatureName);
            }
            else
            {
                VariableTemperature = variableTemperature;
            }

            if (VariableTemperature == null)
                throw new Exception(string.Format("Could not get the '{0}' variable.", variableTemperatureName));

            Log(string.Format("Successfully aquired '{0}' variable.", variableTemperatureName));

            var variableSpeed = DataSource.GetVariables().FirstOrDefault(v => v.GetName() == variableSpeedName);
            if (variableSpeed == null)
            {
                Log(string.Format("Did not find variable '{0}'. Creating one...", variableSpeedName));
                VariableSpeed = DataSource.CreateVariable(variableSpeedName);
            }
            else
            {
                VariableSpeed = variableSpeed;
            }

            if (VariableSpeed == null)
                throw new Exception(string.Format("Could not get the '{0}' variable.", variableSpeedName));

            Log(string.Format("Successfully aquired '{0}' variable.", variableSpeedName));

        }

        /// <summary>
        /// Event to connect to selected serial port.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ConnectButtonOnClick(object sender, EventArgs e)
        {
            try
            {
                var com = portComboBox.SelectedValue.ToString();
                var baudRate = Int32.Parse(baudRateComboBox.SelectedValue.ToString());

                if (!serialPort.IsOpen)
                {
                    connectButton.Enabled = false;
                    connectButton.Text = "Connecting...";

                    Log(string.Format("Attempting connection to {0} with baud rate {1}...", com, baudRate));
                    serialPort.PortName = com;
                    serialPort.BaudRate = baudRate;

                    serialPort.Open();

                    connectButton.Enabled = true;
                    connectButton.Text = "Disconnect";

                    Log(string.Format("Successfully connected to {0}.", com));
                }
                else
                {
                    connectButton.Enabled = false;
                    connectButton.Text = "Disconnecting...";

                    serialPort.Close();

                    connectButton.Enabled = true;
                    connectButton.Text = "Connect";

                    Log(string.Format("Disconnected from {0}.", com));
                }
            }
            catch (Exception ex)
            {
                Log(string.Format("ConnectButtonOnClick - ERROR: {0}", ex));
            }
        }

        /// <summary>
        /// Event raised when data is received via serial port.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SerialPortDataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            try
            {
                var serialValues = serialPort.ReadLine()?.Replace("\r","").Split(';').ToArray();
                var temperature = float.Parse(serialValues[0]) / 100;
                var speed = float.Parse(serialValues[1]) / 100;

                VariableTemperature.SaveValue(temperature);
                VariableSpeed.SaveValue(speed);
            }
            catch (Exception ex)
            {
                Log(string.Format("SerialPortDataReceived - ERROR: {0}", ex));
            }
        }

        /// <summary>
        /// Log activity on screen.
        /// </summary>
        /// <param name="text"></param>
        private void Log(string text)
        {
            text += Environment.NewLine;
            SetText(text);
        }

        /// <summary>
        /// Set text on text box.
        /// </summary>
        /// <param name="text"></param>
        private void SetText(string text)
        {
            if (this.textBox.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(SetText);
                this.Invoke(d, new object[] { text });
            }
            else
            {
                this.textBox.Text += text;
                textBox.SelectionStart = textBox.Text.Length;
                textBox.ScrollToCaret();
            }
        }

        /// <summary>
        /// Close serial port.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnClosing(object sender, FormClosingEventArgs e)
        {
            if (serialPort.IsOpen)
                serialPort.Close();
        }

        /// <summary>
        /// Set text callback.
        /// </summary>
        /// <param name="text"></param>
        delegate void SetTextCallback(string text);

        #endregion
    }
}
